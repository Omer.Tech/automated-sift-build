
<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/Omer.Tech/automated-sift-build/logo.png">
    <img src="./logo.png" alt="Logo" width="600" height="400">
  </a>

  <h3 align="center">Automated SIFT Build</h3>

  <p align="center">
    Deploy a Google Cloud Instance utilizing an Ubuntu machine with the SIFT toolset installed for forensic analysis
    <br />
    <a href="https://www.sans.org/blog/investigate-and-fight-cyberattacks-with-sift-workstation/"><strong>Explore SANS SIFT »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/omer.tech/automated-sift-build/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/omer.tech/automated-sift-build/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Contact](#authors)



<!-- ABOUT THE PROJECT -->
## About The Project

This work is for our CYB 473 Final Project at National University's School of Engineering. We created an Ansible playbook to deploy an instance of Ubuntu on the Google Cloud Platform. We then added extra logic for the Ubuntu instance to download and install the latest version of the SANS SIFT Toolset. The SANS SIFT Toolset can be used for all types of digital forensics and analysis.
<br>
This project was built using the infrastructure-as-code methodology.

### Built With

* [Ansible](https://ansible.com)
* [Google Cloud Platform](https://cloud.google.com)
* [SANS SIFT](https://https://digital-forensics.sans.org/community/downloads)
* [SANS SIFT CLI](https://github.com/teamdfir/sift-cli)




<!-- GETTING STARTED -->
## Getting Started

### Prerequisites
As mentioned before, this project was built with the infrastructure-as-code methodology. The only requirements to use this tool are a Linux or Mac (Unix) system, Ansible, and an account on Google Cloud Platform.

The only minor challenge for this is  creating a serice account, and authenticating that service account to Google Cloud Platform in order to build our cloud instance. Luckily, Google has great documentation on how to do this. If you don't already have a setup to authenticate to GCP, just follow these documents! <br>
* [Creating a service account](https://developers.google.com/identity/protocols/oauth2/service-account#creatinganaccount) <br>
* [Setting up Authentication](https://support.google.com/cloud/answer/6158849?hl=en&ref_topic=6262490)

### Installation

1. Clone and setup the repo
```sh
git clone https://gitlab.com/omer.tech/automated-sift-build.git
sudo chmod 755 -R +x ./automated-sift-build
cd automated-sift-build
```

2. Using your favorite text-editor, open roles/common/tasks/main.yml

3. For the variable "gcp_cred_file" replace our path with the path to your .json file from the prerequisites section


<!-- USAGE EXAMPLES -->
## Usage

Once these steps are complete, we are ready to deploy! <br>
```sh
ansible-playbook sift-build.yml
```

<!-- CONTACT -->
## Authors

Omer Turhan | omer.turhan@outlook.com <br>
Justin Bunker | khronosknows@gmail.com<br>
Patrick Walker | spamalot3651@gmail.com<br>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->